from django.shortcuts import render,  HttpResponse
from django.http import JsonResponse
from .models import Suhu

def index(request):
    return render(request, 'lab1.html')


def temp(request, temp=None):
	if temp != None:
		suhu_obj = Suhu(temp=temp)
		suhu_obj.save()
		return  JsonResponse({'suhu':suhu_obj.temp})

	else:
		suhu_obj = Suhu.objects.all()
		print(suhu_obj[0].temp)
		suhu_list = [i.temp for i in suhu_obj]
		suhu_list.reverse()
		return  JsonResponse({'suhu_list':suhu_list})
