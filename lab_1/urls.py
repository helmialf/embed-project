from django.urls import re_path, path
from .views import index, temp
#url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    path('temp/', temp, name='temp_no_input'),
    path('temp/<int:temp>', temp, name='temp'),
]
